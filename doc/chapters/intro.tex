\chapter{Introduction}
\label{ch:intro}

This chapter describes the problem and goal of this thesis. The main goal of this thesis is to conceive a solution to a well-studied problem in \cv{}: \pe. Our take on the problem is a bit presumptuous, since our goal is to recover 3D information from 2D information: without any non-trivial assumptions this task is obviously impossible. We try to be as open as possible and describe (most of our) others (failed) attempts to reach a solution.

\section{Problem statement}
\label{sec:problem_statem}

(3D) \pe{} is a problem in \cv{} research area where we want to retrieve location (and orientation) of the most \textit{important} points of an entity (in 3D space). By \textit{important} points, we mean points that (semi-)uniquely identify the location (and orientation) of the entity in world space: \eg{} body joints in mammals, corners in urban buildings ... It's important to note that the importance of a point belonging to an entity is evaluated completely arbitrarily, thus it may be subject to bias (see \prettyref{sec:Discussion_bias}). In evaluation phase we followed related work guidelines and evaluated our method on the same points as other approaches.\\

\textit{Human} \pe{} (see \prettyref{sec:background_pose}) is just dealing with human bodies (where the entity is the dressed human, not considering objects that may be held/touched by the human). We concentrate our effort on this problem, but we note that, although our method is developed and evaluated on human-body datasets it may be applicable to any topological space homeomorphic to a human body, \ie{} with genus $= 0$.\\

Finally, we categorize the problem further by absence of information: the \textbf{in-the-wild} \pe{} problem assumes to know nothing about the cameras (location, orientation, focus ...). We try to solve this categorization of the problem. We stress the fact that our method is an \textit{exploratory} analysis of this problem: the work is by no means finished and can be explored further.

\subsection{Setup}
\label{sec:problem_setup}

A single human moves around a pre-determined area and its movements are captured (synchronously) by a number of (fixed) cameras (usually placed in convenient angles, \eg{} at $2 \pi / C$ radians between them, where $C$ is the number of cameras). Cameras look in the direction of the human and are fixed throughout the capture process.\\

Typically infrared or structured light sensors (\citet{H36M}) are placed on the following body parts of the human:

\begin{enumerate}
    \item right foot, knee, buttock
    \item left foot, knee, buttock
    \item pelvis
    \item back, neck, head, nose
    \item right shoulder, elbow, hand
    \item left shoulder, elbow, hand
\end{enumerate}

totaling between 15 and 17 in most used datasets (\citet{H36M}, \citet{cmu_panoptic}, \citet{mpiII_dataset}, \citet{skipose1}).

\subsection{Goal}
\label{sec:problem_goal}

Given a set of (synchronous) pictures depicting the human in question, the goal is to retrieve the pose of the human (in world coordinates). In literature (see \citet{hartley_zisserman_2004} for an excellent reference), this is referred to as \textbf{multi-view} \pe, \ie{} we take pictures with the same camera at the same subject and want to reconstruct the subject pose in the original reference frame, without knowing the location, nor the orientation of the places where we took the pictures.

\subsection{Chosen techniques}
\label{sec:problem_how}

We make certain assumptions and thus solve a particular case of the \pe{} problem. We arbitrarily explore the problem using novel \dl-based techniques. In particular

\begin{itemize}
    \item we make use of fully-connected \nn{} as the basic layer of most of our architectures
    \item to propagate as much information as possible, we employ residual blocks
\end{itemize}

It's important to note that \dl-based approaches in \cv{} have gained a lot of success during latest years but nevertheless, it seemed a practical idea to not feature-engineer manually the model, rather let it \textit{train} autonomously. However the model is instructed to produce particular outputs, that are subsequently processed through classical \cv{} techniques: \dl{} serve the purpose of building a latent representation of the input and then translate that to a manageable and representable form.\\

It goes without saying that, in-depth analysis of all the gates of the \nn{} has not been carried out, so we cannot really be sure to be far from pitfalls common to \nn{}, e.g shortcut learning (\citet{shortcut_learning}), generalization to out-of-distribution data (\citet{recognition_cow}) and so on (\citet{ai_fallacies}).\\

Finally, because of the nature of the chosen problem difficulty, we cannot assume to have \gt{} data: that means that all the training of the \nn{} is done unsupervised-ly, using proper self-consistency losses.

\subsection{Assumptions}
\label{sec:problem_assumptions}

We apply the common assumptions when dealing with digital camera projections (pinhole) with occasional problem simplifications as below:

\begin{itemize}
    \item we model the projection of a 3D point (in world space) to a 2D point (in image space) via the usual pinhole camera projection. Additionally, we can employ a weak-projection model (even orthogonal)
    \item cameras share a (common) intrinsic matrix: this is a necessary assumption for a unsupervised training procedure
    \item cameras look at a pre-defined body point (e.g pelvis). When this assumption is applied, we re-sample the dataset and evaluate the method on the re-sampled data. We admit that this is a strict assumption but it can be lifted in particular situations, as shown in \prettyref{subsec:conc_contribs_var2}
    \item since the training procedure does not rely on \gt{} data, we have to establish a correspondence between predicted pose and \gt{} one: an assumption regarding the location of the origin must be included. Often (see \citet{CanonPose}) the pelvis is assumed to have world coordinates $=(0,0,0)$
\end{itemize}

\section{Motivation}
\label{sec:motivation}

\textit{Solving} the \pe{} problem can mean a lot of different things depending on the assumptions that one makes about the information available.\\

In a perfect world scenario, with no noise, the problem is clearly solved by triangulation via DLT; other methods can be used but are less straight-forward.\\

In a real-world scenario, with noisy measurement and information on the cameras (orientation, location, focus ...) the problem is considered solved by a number of methods (including triangulation). Although the solution to this problem is available, there is practical use for it: in-the-wild \pe{} is still considered unsolved because of the too many constraining assumptions that need to be made.\\

From a theoretical point of view, the absence of a good and practical method to solve a problem can be enough motivation towards research in the area. But we note that from a applicative point of view, there is interest as well (see \prettyref{sec:motivation_apps}).\\

Moreover the method should \textit{learn} by self-supervision \ie{} without seeing \gt{} data (\citet{rip_van_Winkle_razor}): one cannot assume to have a labeled dataset for all in-the-wild scenarios. For this reason, we also want to use a model-free approach (we do not want to add biased (see \prettyref{sec:Discussion_bias}) information regarding joints (relative) position.\\

We hopefully motivated \textit{research} on the problem, but what about its \textit{feasibility}: is it sufficient to research a solution in order to find one?

\subsection{Feasibility}
\label{sec:motivation_feasibility}

Regarding the feasibility of such method, one can \textit{suppose} that it's indeed feasible: human eyes correctly perceive and reconstruct the pose of (human) bodies (up to a constant scaling factor (see \prettyref{subsec:background_dolly})). This, apparently, is \textit{good enough} to perform \say{hard} tasks, such as driving (see \citet{musk_hard}). Of course, humans being good at it, does not imply that the problem is solvable by mathemaical methods.

\subsection{Applications}
\label{sec:motivation_apps}

Applications of such method range from autonomous driving to in-house gesture recognition. In particular in (most of) real world scenarios, solving \pe{} problem can

\begin{itemize}
    \item help autonomous-driving solutions (formally \textbf{action recognition and prediction}, as researched in \citet{Angelini2018ActionXPoseAN}, \citet{Markovitz_2020_CVPR}, \citet{DBLP:journals/corr/abs-1801-07455})
    \item identify humans in dangerous situations (construction work, mountains ...)
    \item lead to more practical ways to play indoor games, as explained by \citet{DBLP:journals/corr/abs-1906-09955}
    \item identifying and improve human body motion in high-performance environments (\eg{} competitive sports)
    \item AR and VR, as motivated by \citet{DBLP:journals/corr/abs-1812-02246}, \citet{DBLP:journals/corr/abs-2008-04524}
\end{itemize}

\section{What comes next}
\label{sec:overview}

This thesis is structured as following:

\begin{itemize}
    \item \prettyref{ch:background} introduces all the necessary frameworks and tecniques that one should be familiar with to understand this thesis
    \item \prettyref{ch:related_work} surveys the field and explains the differences between the proposed method and related approaches
    \item \prettyref{ch:conc_contribs} delineates the proposed method from a theoretical point of view
    \item \prettyref{ch:prop_pipeline} describes the method pipeline, explaining all details of input processing
    \item \prettyref{ch:empirical_results} lists the empirical results and evaluations of the method \wrt the dataset
    \item \prettyref{ch:discussion} discusses the results obtained and provides suggestions for future developments
\end{itemize}

\section{Reproducibility}
\label{sec:overview_avail}

The method and results presented in the following pages can be considered a proof-of-concept. From what we know, our model may suffer from many \nn{} \textit{diseases} that are not visible from results (but only by peeking one-by-one the model weights). This may seem pessimistic, but it's not. Humans can perform many \say{hard} tasks without help of any other input channel other than visual information, and \say{hardness} often means impossible in mathematical terms.\\

This is not a publication, nevertheless it should be as open as possible to being reproducible. With that word we mean the confirmation of final evaluation results on the same datasets we used. Ideally, it's as simple as running a 1-line command; in practice it's much more laborious, but we tried to make it as accessible as possible.

\begin{itemize}
    \item regarding the code-base, it \href{https://github.com/sirfoga/learnable-triangulation-pytorch}{has been open-sourced}. We forked \citet{paper:learnable_triangulation}'s code, since we rely on the same dataset for evaluation (thus saving time on pre-processing, loading, setup steps) and we committed code to that same code-base since then: if you wish to clone the repository you might want to use \texttt{----depth=1} option in order to avoid downloading all commits
    \item regarding the dataset (see \prettyref{sec:data_dataset}) you might want to send an email (as soon as possible) to \texttt{human@imar.ro}. Having an account at the \href{http://vision.imar.ro/human3.6m/}{official website} is not enough: you have to produce a way for the organizers to trust your application. Once you obtained the data, you need to allocate some time (and resources) to parse the dataset as instructed by \href{https://github.com/anibali/h36m-fetch}{anibali/h36m-fetch}
\end{itemize}

\section{Formalities}
\label{sec:note_form}

\subsection{Abbreviations}
\label{subsec:note_abbs}

\begin{tabular}{@{}rp{35.6em}@{}}
  NN & \nn \\
  CNN & \cnn \\
  ResNet & residual \nn \\
  SOTA & \sota
\end{tabular}

\subsection{Notation}
\label{subsec:note_nots}

\begin{tabular}{@{}rp{35.6em}@{}}
  $\mathcal{L}$ & loss function \\
  \say{we found that} & quotes from a reference \\
  \textit{most of} & informal terminology \\
  \textbf{MPJPE} & important terminology \\
  \texttt{model} & \nn{} block/layer/model
\end{tabular}

\subsection{Diagrams}
\label{subsec:note_diagrams}

There are very few established standards for how to represent notions in \dl{}. In particular, there is no overall accepted standard for visualizing \nn{} architectures. We here introduce our system to visualize blocks and models of a \nn:

\begin{itemize}
    \item rounded-corner boxes contain a transformation that (possibly) modifies its input. Residual connections are depicted like this, since they're stored in memory, even if they are not transformed
    \item rectangles contain a tensor, just for visualization
    \item usual \nn{} layers are written in a bold, 18pt font (parameters are in normal font, 12 pt)
    \item \nn{} activations are in camel case, written in a bold, 18pt font (parameters are in normal font, 12 pt)
    \item $\sim$ describes the dimension of the tensor
    \item \textit{foreach} loops in a range
\end{itemize}

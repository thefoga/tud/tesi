\chapter{Discussion}
\label{ch:discussion}

This chapter discusses the proposed method, after evaluation of results. We will discuss biases, limitations and future improvements. Finally we wrap this thesis up in the conclusive section.

\section{Bias}
\label{sec:Discussion_bias}

\textit{Bias} is an umbrella term describing everything that's (implicitly) input to a model. The more bias, the less variable and generalizable a method is, and viceversa. Analyzing biases thus, is an important area of study of any method that must \textit{transfer} to different-than-training (\eg{} real-world) scenarios. As famously stated \say{there are known unknowns}, \citet{unknown_unknowns}: in this section we will list known biases of our method, but we are sure there are more inside the set of \say{known unknowns}.

\subsection{Choice of joints}
\label{subsec:Discussion_bias_joints}

It's quite difficult to disentangle methods and data in \hpe{} since almost every method (even ours) developed is tested on \say{standard} datasets, which reflect the same set of joints. What if a method that works using a set of joints, does not work using another set? It would be interesting also to retrieve 3D pose given many possible set of joints.\\

Moreover, the \say{standard} set of joints is not applicable to real-world scenarios of injured humans (possibly with no arms, legs).

\subsection{Occlusions}
\label{subsec:Discussion_bias_occlusions}

Real-world images often contain many (and many type of) occlusions (\eg{} insects, birds, trees ...), which as noted in \prettyref{subsec:background_occlusions}, play a large role in defining the difficulty of the pose reconstruction problem. We haven't seen any of such occlusions in the datasets examined, and thus, methods trained on those datasets may be biased in \textit{not} correctly considering those occlusions.

\subsection{Shape of you}
\label{subsec:Discussion_bias_shape}

Ideally, we want a \hpe{} to reconstruct the pose by recognizing the shape of human body parts: the method should be invariant to the colors of skin and clothes. However, popular datasets include prevalently white people, dressed in uniform-colors clothes. In-the-wild datasets (like \citet{skipose1}) contain humans dressed in various different combinations of colors but we argue it's not sufficient to ensure a recognition-by-shape.

\section{Generalization}
\label{sec:Discussion_gener}

\xmakefirstuc{\wrt} \hpe{} problem, we define a \textit{generalizable} method, one that can successfully reconstructs the pose from previously not seen images and contexts. This feature is highly desirable (and necessary) in deploying \pe{} methods in real-world data.

\subsection{Training poses}
\label{subsec:Discussion_gener_poses}

\xmakefirstuc{\hpe} problem deals with just the pose and does not imply any requirements on the scene background. Therefore, unusual poses (\eg{} upside down, in water ...) should be part of training and evaluation datasets. Else, suitable data augmentation techniques should be applied during the training process.

\subsection{Background}
\label{subsec:Discussion_gener_background}

\xmakefirstuc{\wrt} \hpe{} problem, we define the background as everything that's not part of the human entity (even foreground objects). We argue that, in order to be applied successfully to various backgrounds, during the training process, one should completely remove the background (or vary it with proper random noise (\eg{} Gaussian, Poisson)).\\

What happens instead (\eg{} in \citet{H36M} dataset) is that images background is not changing during recording process. It is arguable (although not proven) that, specifically in the popular \citet{H36M} dataset, camera parameters (intrinsic and extrinsic) can be inferred just from the background, resulting in one of the possible \textit{shortcut} strategies that are definitely not desirable.\\

\citet{paper:learnable_triangulation} tries to solve this problem by applying ground-truth (rectangular) cropping around the human thus

\begin{itemize}
    \item changing camera parameters almost every frame
    \item changing (little) the background around the human
\end{itemize}

\subsection{Image size}
\label{subsec:Discussion_gener_img}

Finally, we pose that image resolution and size should not play any role in the resulted \pe{}. Of course there is a theoretical minimum number of pixels that is necessary to reconstruct the pose (any given pixel can contain a finite amount of information) and an optimal one (e.g \citet{deepcut_bjoern} find the best is $384 \times 384$), but we pose that one should strive to leverage the minimum number of pixels possible.

\section{Robustness}
\label{sec:Discussion_robust}

Ideally the function our model learns is not \textit{chaotic}: a small change in the input image should reflect small change in the output pose. Likewise for small perturbations applied to the joint detections. This property may be referred to as \textbf{robustness}. We have not conducted an in-depth robustness analysis of our method (since our main goal was to provide a proof-of-concept approach), nevertheless we carried out the following experiments perturbing the joint detections.

\subsection{Noise}
\label{subsec:Discussion_robust_noise}

We applied Gaussian noise ($\mu = 0$) to the ground-truth joints (see \prettyref{fig:noised}a for a sample) and run the forward pass to retrieve the pose. Using \vone{} achieved the results shown in \prettyref{tab:noise_results} and \prettyref{fig:noised}b.

\begin{table*}[htp]
\centering
\rowcolors{2}{black!10}{black!40}
\begin{tabular}{*5l} \toprule
    $\sigma$& PMPJPE (mm) \\ \midrule
    0.25 & $32.1 \pm 2.6$ \\
    0.5 & $49.4 \pm 4.1$ \\
    1.0 & $79.3 \pm 5.2$ \\
\bottomrule
\hline
\end{tabular}
\caption{Results of proposed method (\vone{}), after applying noise}
\label{tab:noise_results}
\end{table*}

\begin{figure*}[!h]
    \centering
    \showSubFig{\twoColImgsWidth}{\imgDir/noised.png}{2D detections deformed with Gaussian noise ($\mu = 0, \sigma = 0.25$)} \showSubFig{\twoColImgsWidth}{\imgDir/noised_reconstructed.png}{3D reconstructed pose from 2D detections deformed with Gaussian noise ($\mu = 0, \sigma = 0.25$)}

    \caption{Effect of applying Gaussian noise to ground-truth keypoints: synthetic random sample and pose reconstruction. \xmakefirstuc{\gt{}} joints are colored in \colorbox{red}{red}, predicted in \colorbox{blue}{\textcolor{white}{blue}}}
    \label{fig:noised}
\end{figure*}

\section{Limitations}
\label{sec:Discussion_limits}

We have not tested the method in a real-world scenario, with real-world cameras under real-world environment. Nor we could, nor it would be meaningful to do so: it's not how many \textit{good results}, but the first \textit{bad} result that you get that counts. Many things can go \textit{wrong} in a real-world setting, but since AI is the \text{art of illusion} we limited ourself to report results in standard, controlled, closed datasets. In this section we describe known limitations of our model.

\subsection{Rotation estimation}
\label{subsec:Discussion_limits_rot}

First of all, we note that what we really estimate in \prettyref{subsec:conc_contribs_Rt} is not the absolute camera parameters, we just need a self-consistent set of camera rotations, since we then calculate MPJPE after rigid-alignment with the ground-truth pose (\ie{} PMPJPE). We also note that given an unsupervised training, it's impossible to match the ground-truth camera parameters (since there is any information regarding it). Basically, we achieve (through \dl-based model) what is theoretically possible, given our assumptions. However, this can be seen as a limitation whenever such (absolute) information is required.

\subsection{Translation estimation}
\label{subsec:Discussion_limits_trans}

Likewise for the translation part of the camera extrinsic estimation: because of obvious depth ambiguities (see \eg{} \prettyref{subsec:background_dolly}), we just estimate a self-consistent set of camera translations. Moreover, introducing more cameras (as it's common in other datasets \eg{} \citet{cmu_panoptic} where the number of cameras looking at the subject is around 40) does not resolve these ambiguities. Experiments with 60 cameras, uniformly distributed across the Euler angles as in \prettyref{fig:fake_cam_rand}, do contribute to speed up the convergence and consistency of the solution, but not its absolute correctness (\ie{} loss).

\showFig{\fullImgWidth}{\imgDir/res/fake_cam_rand.png}{A random camera system used for experiments, uniformly distributed on the Euler angles}{fake_cam_rand}

\section{Future improvements}
\label{sec:future}

This chapter suggests possible future developments, based on the results obtained. On a high level, first of all, we should ideally solve every problematic biases noted in \prettyref{sec:Discussion_bias}. Furthermore we should improve the different parts of our pipeline with more suitable techniques. Finally we should strive for generalization accuracy and better out-of-distribution results. In particular\\

\begin{itemize}
    \item given we have access to the test dataset (like any \say{AI} research field) we cannot fully trust the results of methods, as proved by \citet{rip_van_Winkle_razor}
    \item \prettyref{subsec:Discussion_gener_img} brings up the problematic relationship between image size (and resolution) and method results:
    \begin{itemize}
        \item firstly we should estimate (theoretically) what are the minimum requirements (in terms of image resolution and size) to estimate a correct pose
        \item then try to find a model that works (empirically) as near as possible to the theoretical minimum
    \end{itemize}
    \item as noted in \prettyref{subsec:Discussion_bias_shape}, a desirable property of a \hpe{} method is recognition-by-shape, invariant to any color distortion (of human parts and background). A viable method to leverage the shape would be to employ \textit{Fourier descriptors}: one big advantage of such method is that \say{it is possible to capture coarse shape properties with only a few numeric values}, as explained in \citet{Burger2013}
    \item alternative techniques to estimate camera parameters are due a rigorous attempt. In particular the usage of dual-quaternions (see \prettyref{subsec:background_repr_quats}) to represent roto-translations in a integrated way should be taken into consideration
    \item alternative ways of encoding representations seem promising as well (especially graph \nn s (see \prettyref{subsec:related_work_super})): they shall be explored
    \item a comparison of performance across datasets (especially \citet{skipose1}, \citet{cmu_panoptic}, \citet{totalcapture_dataset}, \citet{mpiII_dataset}) is also due. This would also enable to make a full comparison with other competitive methods.
\end{itemize}

\section{Conclusion}
\label{sec:Conclusion}

Finally, we recap what has been describe in this thesis.

\begin{itemize}
    \item we proposed a \dl-based method to solve in-the-wild \hpe{} problem. The method successfully disentangles camera information and pose so it can be applied to many levels of data supervision
    \item we evaluated the proposed method in one of the standard datasets and achieved competitive results
    \item we discussed empirical results, method limitations and future improvements that should be taken into consideration if the method has to be applied in real-world scenarios
\end{itemize}

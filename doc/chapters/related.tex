\chapter{Related work}
\label{ch:related_work}

This chapter surveys related approaches to solve multi-view (human body) \pe{}. We limit the survey to comparable (with proposed method) approaches, thus focusing on \dl-based models.

% todo more (like 20/40) works, especially table 8 (lifting based) from survey! Or from https://github.com/zczcwh/DL-HPE/tree/main/3DHPE#--single-view-multi-person-3d-hpe
% how does your method improve existing ones?, devi spiegare cos'hanno gli altri che sbagliano (e te no)

% \section{Classic \cv{} approaches}
% \label{sec:background_classic_cv}
% \todo{} ? \citet{Affective_Growth_CV}

\section{\xmakefirstuc{\dl} techniques for multi-view 3D \hpe{}}
\label{sec:background_dl_in_cv}  

Referring to \citet{dl_hpe_survey} taxonomy, this section compares some of the most similar approaches to solve single \hpe{} from monocular RGB cameras. As a reference, our method can be located in the \textbf{in-the-wild multi-view single-person 3D model-free lifting-based} category. We divide related works based on the data used in the loss calculation (\ie{} directly, indirectly, without using ground-truth data).

\subsection{Supervised learning}
\label{subsec:related_work_super}

This section presents some of the milestones that led to modern formulations and solutions of the human \pe{} problem using \textit{supervised} methods, \ie{} knowledge of \gt{} data is available (and used) during training-time of the model.\\

\citet{deepcut_bjoern} (and its successor \citet{deepercut_bjoern}) have been \say{one of the earliest (2D) bottom-up approaches} to solve multi-person \pe{}. Both approaches don't require a \textit{people detector}, instead

\begin{itemize}
    \item employ a \cnn{} (\citet{vgg}-based) architecture to detect all human joints (from all people depicted)
    \item assign each joint to a person via $0/1$ variables optimization
\end{itemize}

Being computationally expensive, \citet{deepercut_bjoern} aims at improving performance.\\

Regarding 3D \pe{}, \citet{MartinezHRL17} was also among the earliest and most straightforward approaches for single-person \pe{}. In fact, as we will explain in \prettyref{sec:conc_contribs_model}, we follow their guidelines architecture-wise. \citet{MartinezHRL17} focused on directly regressing (from a single view) the 3D pose. In order to achieve so, they feed the 2D detections into a residual (with 2 inner dense layer) block. However there are some big differences in the 2 models (ours and \citet{MartinezHRL17}):

\begin{itemize}
    \item \citet{MartinezHRL17} solves \pe{} directly from the 2D detections; the output of our model is not a pose, but camera information
    \item we consider a multi-view setting
    \item they supervise using \gt{} data
\end{itemize}

Regarding single-person multi-view \pe{}, a number of methods propose to use formulations of various consistency constraints and geometry-aware techniques.\\

\citet{DBLP:journals/corr/abs-1901-04111} devise \say{a multi-way matching algorithm to cluster the detected 2D poses in all views} thereby solving the problem of \say{cross-view correspondences among noisy and incomplete 2D pose predictions}. They decided not to use the pictorial structure model since it's quite computationally expensive. Indeed, they start from the 2D detections (inferred through a \cnn-based model), cluster the detections belonging to the same person and finally triangulate to reconstruct the 3D poses.\\

\citet{ieee_8954286} use (indirectly \ie{} they employ pre-trained backbones) just 2D keypoints supervision and encode the pose information via a geometry-aware auto-encoder. They first infer the 2D skeleton maps from raw images, then encode the skeleton under the specific viewpoint and pose a representation consistency constraint by interpolating from one viewpoint to the others.\\

\citet{epipol_transformer} find the correspoding point of a point in another image via a \citet{attention_you_need}-based mechanism: given a learned feature of a point representing the \textit{query}, they match the point via a similarity distance defined on the points of the corresponding epipolar line in the other view. The computed \textit{attention} weights are used to fuse feature and compose \say{thus leading to a 3D-aware feature} of the point.\\

\citet{paper:learnable_triangulation} introduces two end-to-end differentiable triangulation methods which are trained on a labeled training dataset, assuming to know camera information.

\begin{itemize}
    \item their \textit{algebraic} method detects the joints in the pictures and triangulates via DLT (using confidences)
    \item the \textit{volumetric} method instead, produces heatmaps of the joints in the pictures, which are used to produce aggregated volumes in world space. The volumes are fed into a 3D \cnn, whose \texttt{soft-argmax}-ed result is the \pe{}
\end{itemize}

\citet{paper:edo} successfully (and quickly) disentangle feature maps (of the pose) from camera viewpoints: they fuse representations of each input image into a latent space (via 1D convolutions) using \citet{feature_transform_layers}, and \say{condition the latent code on the known camera transformation to decode it back to 2D images}. They stress the efficiency and speed of their work, in fact it can be applied in real-time scenarios.\\

Finally, representing the pose as a graph (see \prettyref{sec:background_pose}) has its benefits, as confirmed by \citet{pose2mesh_gcn} approach: they applied graph \cnn s and supervise \say{normal vectors of an output mesh surface to be consistent with ground-truth}.

\subsection{Weakly-supervised learning}
\label{subsec:related_work_weak}

Whenever \say{noisy, limited, or imprecise sources are used to provide the supervision signal} (\citet{weak_supervision}) we apply \textit{weak-supervision}. In the context of \hpe{} this often translates to incorporating structural or scene priors into the model, as the following works show.\\

\citet{Cross_View_Fusion} can be considered a lifting-based approach that leverages the geometric structure of the human body. They first compute the heatmaps of the joints in each image (via a \cnn{}), then fuse all heatmaps (of the other views) and recover the 3D pose via a recursive formulation of the widely used pictorial structure model: they discretize \say{the space around each joint location (estimated in the previous iteration) into a finer-grained grid using a small number of bins}, thereby improving both on precision and efficiency.\\

\citet{helge_ski} leverages the little annotated data available and multi-view consistency (only at training time) to predict the pose from a single image (at inference time). They train the \cnn-based network \say{using a novel loss function that adds view-consistency terms to a standard supervised loss evaluated on a small amount of labeled data and a regularization term that penalizes drift from initial pose predictions}. We somehow share the method of computation of the multi-view consistency loss: indeed, they first rotate the pose (assuming a rotation around the root joint) and normalize it (by its Frobenius norm) to compare with the pose estimated in the other view.\\

\citet{weakly_Zhou_17} uses a mixture of 2D and 3D ground-truth labels that to supervise reconstructed 2D heatmaps and 3D pose. As usual, they feed the raw images into a \cnn{} to produce the 2D joints heatmaps. A depth-regression module is used to regress depth from the 2D detections. Overall, their method seems to be usable in-the-wild, where there is little to none supervision.\\

\citet{umar_weakly} leverages the few 2D joints annotations available and multi-view consistency (thereby penalizing inconsistent poses when re-projected to other views) to produce \say{latent depth maps} and a pose representation in a so-called 2.5 dimensional space, consisting of a the $XY$ joints coordinates and a relative (to the root joint) depth estimate. Obviously the reconstructed pose \say{does not account for scale ambiguity present in the image which might lead to ambiguities in predictions}. They also impose a \say{limb length} loss, penalizing \say{the deviation of the limb lengths of predicted 3D pose from the mean bone lengths}.\\

% todo more in page 11 from survey, basically Table 1 of \citet{CanonPose} ?

\subsection{Unsupervised learning}
\label{subsec:related_work_unsuper}

Finally, this section reviews some of the most important methods solving \hpe{} when no ground-truth labels are available.

\citet{Unsupervised_3D_Pose_Estimation} randomly projects the reconstructed pose (from a single image) to \say{synthetic} cameras, re-lifts it in world coordinates and re-project it in the original camera space, thus providing self-supervision. However, the self-supervision in the lift-reproject-lift process is not enough to provide an accurate pose reconstruction: they self-train a 2D pose discriminator \say{that takes as input a 2D pose and outputs a probability} $\in [0, 1]$, representing the probability of it being a \say{real 2D pose $r$ (target probability of $1$) and fake (projected) 2D pose
y (target probability of $0$)}.\\

The \say{EpiloparPose} method described in \citet{epilopar_pose} \say{estimates 2D poses from multi-view images, and then, utilizes epipolar geometry (see \prettyref{sec:background_epipolar}) to obtain a 3D pose and camera geometry which are subsequently used to train a 3D pose estimator}. During training they impose a Huber-kind of loss on the normalized re-projection, while during inference they only assume to have a single image.\\

\citet{can_3d_from_2d} randomly projects an (unsupervised-ly) estimated 3D pose back to 2D, which is then \say{evaluated by a discriminator following adversarial training approaches}. Altough pose is estimated without \gt{} data, scene is supervised by constraining the camera rotation axis (used for the re-projection).\\

\citet{CanonPose} delivers a robust in-the-wild method that works by disentangling the 2D pose (in image space) into the 3D pose (in world space) and camera rotation. The only constraining assumption is using orthogonal-projection cameras. The method is modular (can be embedded in any other pipeline) and trainable without \gt{} labels.

\chapter{Background}
\label{ch:background}

This chapter introduces all the necessary frameworks and tecniques that one should be familiar with to understand this thesis, in particular representations for rigid body transformations.

\section{Reference frames}
\label{sec:background_coords}

A number of coordinate systems can be defined in a multi-camera capture system.

\textbf{World space} originates arbitrarily in any given point of the real world and can be used as (unique) reference frame to locate points in the real world ($\in \mathbb{R}^3$).

For any camera, we define the \textbf{camera space} as the space of points as seen by the camera ($\in \mathbb{R}^3$).

Projecting points from camera space to the camera plane means moving from camera coordinates to \textbf{image coordinates} ($\in \mathbb{R}^2$).

\section{Cameras}
\label{sec:background_cameras}

A (simplified) model of a (digital) camera is employed throughout this thesis called \textbf{pinhole} (camera model):

\begin{itemize}
    \item transformation from world coordinates to camera coordinates is done via the \textbf{extrinsic matrix} of the camera. It specifies the rotation $R$ and location $\boldsymbol{t}$ of the camera in world coordinates
    
    \begin{equation}
    \label{eq:extrinsic}
        E = [ R \, |\, \boldsymbol{t}] = 
        \begin{bmatrix}
        r_{1,1} & r_{1,2} & r_{1,3} & t_1 \\
        r_{2,1} & r_{2,2} & r_{2,3} & t_2 \\
        r_{3,1} & r_{3,2} & r_{3,3} & t_3 \\
        \end{bmatrix}
    \end{equation}

    \item projection of points in camera coordinates to image coordinates is done via the \textbf{intrinsic matrix}, that specifies the focal length $f$, shear $s$ and principal point $(x_0, y_0)$ of the camera.

    \begin{equation}
    \label{eq:intrinsic}
    K = \begin{bmatrix}
        m_x f & s & x_0 \\
        0 & m_y f & y_0 \\
        0 & 0 & 1
        \end{bmatrix}
    \end{equation}
\end{itemize}

Combining the 2 matrices is equivalent to project a point from world space to image space via the projection matrix $P$

\begin{align}
\label{eq:proj_camera}
P &= \overbrace{K}^{\text{intrinsic matrix}} \times \overbrace{[R \mid  \mathbf{t}]}^{\text{extrinsic matrix}} \\ &=
\overbrace{
    \underbrace{
        \left (
        \begin{array}{ c c c}
            1  &  0  & x_0 \\
            0  &  1  & y_0 \\
            0  &  0  & 1
        \end{array}
        \right )
    }_{\text{2D translation}}
    \times
    \underbrace{
        \left (
        \begin{array}{ c c c}
        f_x &  0  & 0 \\
            0  & f_y & 0 \\
            0  &  0  & 1
        \end{array}
        \right )
    }_{\text{2D scaling}}
    \times
    \underbrace{
        \left (
        \begin{array}{ c c c}
            1  &  s/f_x  & 0 \\
            0  &    1    & 0 \\
            0  &    0    & 1
        \end{array}
        \right )
    }_{\text{2D Shear}}
}^{\text{intrinsic matrix}}
\times
\overbrace{
    \underbrace{
        \left ( \begin{array}{c | c}
        I & \mathbf{t}
        \end{array} \right )
    }_{\text{3D translation}}
    \times
    \underbrace{
        \left ( \begin{array}{c | c}
        R & 0 \\ \hline
        0 & 1
        \end{array} \right )
    }_{\text{3D rotation}}
}^\text{extrinsic matrix}
\end{align}

It's helpful to consider the case of a camera \textit{looking} at the origin: it is rotated such that the origin projects (in camera space) to a point whose only non-zero coordinate is the $z$ one. To achieve so (starting from any camera rotation and position) we need to

\begin{enumerate}
    \item find (via \eg{} Rodrigues rotation formula, see \citet{rod_rot_formula} for a detailed analysis) the rotation matrix $R_r$ to align the (camera-space-projected) origin to $z$ axis
    \item apply the rotation to get the new extrinsic matrix
    \begin{equation}
    \label{eq:resampling_cam_ext}
    E' = E \times R_r
    \end{equation}
\end{enumerate}

\subsection{Other models}
\label{sec:background_cam_other_proj}

\begin{itemize}
    \item assuming the object depth (as seen by the camera) is very small (relative to the camera distance from object), a \textbf{weak} projection camera can be used to model the projection: before projecting the points (like in the pinhole model), all points of the body are projected orthogonally on a \textit{reference plane}
    \item \textbf{orthogonal} projection is a special case of \textbf{weak} projection: when the camera lies on the reference plane.
\end{itemize}

\section{\xmakefirstuc{\pe}}
\label{sec:background_pose}

A \textbf{body} can be seen as a undirected graph $\mathcal{G} = (\mathcal{V}, \mathcal{E})$ where

\begin{itemize}
    \item $\mathcal{V}$ is the set of \textbf{joints} with coordinates in a (euclidean) space. In animals the joints are marks on the body chosen arbitrarily (usually the head, knees, shoulders \ldots)
    \item $\mathcal{E}$ is the set of edges between $v \in \mathcal{V}$. In animals $\mathcal{E}$ can be seen as the set of the bones connecting the joints. In this thesis, we are interested only in the joints, so $\mathcal{E} = \emptyset$.
\end{itemize}

\ie{} we split the human body into \say{a collection of $N$ body parts}. Our approach shares some similarity with the \textbf{pictorial structure model} (a volumetric probabilistic approach to model relative relationships between multiple body parts). As first demonstrated by \citet{pic_struct_model}, this model can represent successfully the human pose even in a 3D setting.\\

The \textbf{pose} is defined to be the set of coordinates of the joints of a body. Given a body with $J$ joints ($\mid \mathcal{V} \mid = J$), \pe{} maps the body to a vector $\in \mathbb{R} ^ {3 \times J}$, the locations of the joints in world coordinates.

Straightforwardly \hpe{} is concerned with the pose of humans. This thesis assumes there is only 1 human involved in the scene: formally we're interested in solving the \textbf{single-person} \hpe{} problem.

\subsection{Multi-view setup}
\label{subsec:background_multi_view}

A body is placed within the field of view of a number of cameras. Pictures are taken at same instant, while the body may move. This is in contrast with the \textbf{single-view} setup, where only one camera is used to retrieve the 3D pose.

\subsection{Wild setup}
\label{subsec:background_wild}

\xmakefirstuc{\pe{}} \textbf{in the wild} limits the information available to the method: cameras extrinsic matrix and intrinsics are not known.

\section{Non-triviality of the problem}

\xmakefirstuc{\hpe{}} is, by far, a solved problem, mainly because of the pletora of smaller issues that hinder the solution. In this section we will describe some of them (mainly regarding our approach).

\subsection{Occlusions}
\label{subsec:background_occlusions}

Since our method leverages the body projections (on some camera planes, just like geometric tomography introduced by \citet{gardner_tomo}) occlusions (\ie{} obstruction of view of a 3D point by an obstacle) are a serious problem when dealing with the single-view (or even \textit{few}-view) setups.\\

\textbf{Self-occlusions} (\ie{}) are also a frequent source of mis-alignment in \pe{}, and can only be solved by using an appropriate (usually very large) number of cameras, placed at convenient angles. A clear example of this kind of occlusion can be seen in \prettyref{fig:sample_2d_detections}d. Since this constraint cannot be applied in the wild, we note that our method will suffer from this source of error, but without prior knowledge (see \prettyref{sec:Discussion_bias}) it would be impossible not to be prone to this type of mistakes.

\subsection{Dolly zoom effect}
\label{subsec:background_dolly}

This phenomenon appears when camera extrinsic and intrinsic matrices are tweaked while keeping the chosen subject the same size. It's always possible to do so and it's one of the source of error when estimating the pose without supervision in world space. It is then necessary to perform a rigid transformation (rotation and scaling) before evaluating the fit.\\

Depth ambiguities can also arise from this problem, as \prettyref{fig:in_cam_ambig} demonstrates.

\showFig{\fullImgWidth}{\imgDir/res/incam.png}{Scale ambiguities are a direct consequence of unsupervised pose reconstruction. This is an example showing the reconstructed pose in camera space in \colorbox{red}{red} \vs{} \gt{} pose in camera space in \colorbox{blue}{\textcolor{white}{blue}}}{in_cam_ambig}

\section{Epipolar geometry}
\label{sec:background_epipolar}

The term \textit{epipolar} has its roots in the Greek and Latin language:

\begin{itemize}
    \item the prefix \textit{epi} roughly means \say{space around}
    \item the suffix \textit{polar} comes from the old French (taken from the Latin) and refers to a pole, axis
\end{itemize}

Indeed epipolar geometry is concerned about how a point, as seen in one camera, is projected to another one. With reference to \prettyref{fig:epip_geom}, all points in the \textbf{epiline} $OX$ project to the same point in the left camera, while being projected to different points in the right camera. The \textbf{epiplane} is the plane defined by a 3D point and the two camera optical centers $O_L, O_R$, whereas the \textbf{epipoles} $e_L, e_R$ are the two intersections between the line $O_L, O_R$ and the respective image planes.\\

Clearly, if the relative roto-translation matrix is known, one can assume to know how a point, as seen in one camera is projected to the other camera.

\showFig{\fullImgWidth}{\imgDir/1920px-Epipolar_geometry.svg.png}{Epipolar geometry, \copyright{} Arne Nordmann}{epip_geom}

\section{Triangulation}
\label{sec:background_pure_cv}

Given 2D detections (in image space) coming from (at least) 2 cameras, the process of finding the corresponding point (in world space, $\in \mathbb{R} ^ 3$) is defined as \textbf{triangulation}.\\

Given no-noise setup, only one solution exists. In real-world scenarios, this problem can be cast to an optimization problem and solutions can be found by solving a linear system of equations or, more commonly, a set of similarity relations (via direct linear transformation \aka{} \textbf{DLT}).

\section{Representations for rigid body transformations}
\label{sec:background_repr}

Since representing a roto-translation transformation is at the core of our proposed method, this section delineates some of the most robust ways to represent such types of transformations. We chose to sub-divide the problem into

\begin{itemize}
    \item rotation representation in 3D. We denote with $X = SO(3)$, the set of (legal) 3D rotations. Moreover $I$ denotes the identity rotation, \ie{} $R = IR, \forall R \in SO(3)$.
    \item translation $t$ representation: where we try to regress the translation vector $\in \mathbb{R}^3$ directly or through an embedding
\end{itemize}

\subsection{Euler angles}
\label{subsec:background_repr_eulers}

Euler angles have been introduced by \citet{euler_angles} to represent the orientation of a rigid body \wrt{} a fixed coordinate system. To express a rotation $\in SO(3)$ it's always sufficient to use 3 chained elemental rotations (\ie{} rotations around the axes of a coordinate system) as proved by \citet{elem_rotation}, and that's what Euler angles are used for: they represent a rotation by 2 composition of 3 elemental rotations. \textbf{Proper Euler angles} are the ones included in the set $\{ z-x-z, x-y-x, y-z-y, z-y-z, x-z-x, y-x-y \}$.\\

However this representation is not suitable for our method, since it contains discontinuities, and thus, are difficult to get learned by a \nn. In particular they have interpolation problems \say{near \href{https://en.wikipedia.org/wiki/Gimbal_lock}{gimbals lock} regions} (see \citet{Kenwright_double_quats}).

\showFig{\fullImgWidth}{\imgDir/eulers.png}{Visualization of discontinuities in 3D rotation representations, adapted from \citet{6d_mat_param}}{eulers_disc}

\subsection{Axis-angle}
\label{subsec:background_repr_aa}

\begin{theorem}[Euler's rotation theorem]
    \label{thm:euler_rot_th}
    \xmakefirstuc{\wrt{}} a 3D point of a rigid object, a rotation can be represented as an axis (the rotation axis) and a scalar (the angle of the rotation). This is often called \textbf{axis–angle representation}.
\end{theorem}

It is therefore possible to encode a rotation in a pair axis-angle. Axis-angle representation has been used in \citet{mahendran20173d}. However this way of representation suffer from discontinuities at the 180 degree rotations.

\subsection{Quaternions}
\label{subsec:background_repr_quats}

(Unit) quaternions, as introduced by Hamilton (see \citet{quaternions_hamilton}), provide a simple way to encode the rotation information around an axis. By \prettyref{thm:euler_rot_th} any rotation $\in SO(3)$ can be represented as a combination of a vector $\vec{u}$ and a scalar $\theta$. Given the axis

\begin{equation}
\label{eq:quats_1}
\vec {u}=(u_{x},u_{y},u_{z})=u_{x}\mathbf {i} +u_{y}\mathbf {j} +u_{z}\mathbf {k}
\end{equation}

and a rotation angle $\theta$, then the relative quaternion number $q \in \mathbb{R}^4$ is defined to be

\begin{equation}
\label{eq:quats_2}
\mathbf {q} = e^{{\frac {\theta }{2}}{(u_{x}\mathbf {i} +u_{y}\mathbf {j} +u_{z}\mathbf {k} )}}=\cos {\frac {\theta }{2}}+(u_{x}\mathbf {i} +u_{y}\mathbf {j} +u_{z}\mathbf {k} )\sin {\frac {\theta }{2}}
\end{equation}

can be represented by a quaternion. Quaternions have been successfully used to regress rotations (as in \citet{xiang2018posecnn}, \citet{wang2019densefusion}). In particular \citet{Bui_2018} used $\mathcal{L}_2$ loss to learn a rotation. However this representation too suffer from discontinuities (see section 4.1 in \citet{6d_mat_param}).\\

\citet{cliff_quats} presented the notion of \textbf{dual quaternions} which \say{can represent rotations and translations in an integrated fashion}. These numbers consist of a real (modeling a rotation $\in SO(3)$) and imaginary part (modeling a translation $\in \mathbb{R}^3$); they have been proven useful in modeling roto-translations (as \citet{kavan2006dual} do) and do not present differentiability issues. Their usage is a matter of discussion (see \prettyref{sec:future}).

\subsection{Chosen representation: 6D from \citet{6d_mat_param}}
\label{subsec:background_repr_6d}

We chose to directly regress $t$ and combine it with the 6D rotation parametrization from \citet{6d_mat_param} to retrieve the full rigid roto-translation transformation. This approach provided to be flexible enough and model a wide variance of cases (\eg{} when cameras look at the origin, as specified in \prettyref{eq:proj_camera})

\section{Metrics}
\label{sec:background_metrics}

Given the \gt{} pose $G$ and the predicted pose $P$ we follow the literature and take into account the following metrics:

\begin{itemize}
    \item \textbf{MPJPE} as mean per joint position error, \ie{} the mean of all (Euclidean) distances between each pair of predicted and \gt{} joints. For a clue of how this metrics vary based just on axis-rotation, see \prettyref{fig:mpjpe_rot}. Often this metric is calculated relative to a root joint (\ie{} after shifting the predicted pose to the same coordinate of the ground-truth root joint)
    \item in a unsupervised setting, it doesn't really make sense to consider plain MPJPE, since the scale will be obviously off (see \prettyref{subsec:background_dolly}), instead we consider \textbf{NMPJPE}: MPJPE metric after re-scaling the prediction to the \gt{} scale
    \item in the wild \pe{} suffer also from rotation ambiguity: we then calculate \textbf{PMPJPE} (MPJPE metric calculated after Procrustes rigid-alignment (rotation and translation) with the \gt{} pose). We apply \textbf{Kabsch} algorithm (as delineated in \citet{umeyama_ls}) and find the optimal roto-translation via least-squares optimization (using SVD decomposition)
    \item percentage of correct \kps{} (\aka{} \textbf{PCK}) is a parametric metric, \say{used to measure the accuracy of localization of different \kps{} within a given threshold} as explained in \citet{dl_hpe_survey}. There are many variations:
    \begin{itemize}
        \item in PCKh@$h$, $h$ specifies the $h$ percentile of the head segment length
        \item in PCK@$t$, the threshold is the $t$ percentile of the torso diameter
        \item the threshold of PCK@$d$ mm is simply $d$ mm, as used in \citet{CanonPose}
    \end{itemize}
    \item \textbf{PCP} is similar to PCK but uses the limb length as threshold
    \item \textbf{PSS}@k has been proposed in \citet{epilopar_pose} as an alternative method to MPJPE and derivatives. This metric scores \say{the whole pose as a structure} by comparing (via intersection-over-union metric) the reconstructed pose with the correspondence in the $k$-clustered ground-truth poses. \prettyref{fig:cluster_centers} shows the clustered poses of \citet{H36M} dataset.
\end{itemize}

\begin{figure*}[h]
    \centering
    \showSubFig{\twoColImgsWidth}{\imgDir/mpjpe/90.png}{A random \gt{} pose, rotated by 90 degrees along axis $x, y, z$} \showSubFig{\twoColImgsWidth}{\imgDir/mpjpe/180.png}{A random \gt{} pose, rotated by 180 degrees along axis $x, y, z$}
    
    \caption{MPJPE calculation on a random \gt{} pose rotated}
    \label{fig:mpjpe_rot}
\end{figure*}

\showFig{\fullImgWidth}{\imgDir/cluster_centers.pdf}{Cluster centers which represents the canonical poses in Human3.6M ($k = 50$), \citet{epilopar_pose}}{cluster_centers}

\section{\xmakefirstuc{\dl{}} in \cv}
\label{sec:background_dl_in_cv}

Mapping high-dimensional points (such as image coordinates) to a low-dimensional space (such as class predictions) can be cast to a \nn-solvable problem. Especially \cnn-based architectures have been found to be suitable in computer vision domain.\\

The useful (and required) assumptions when modeling the problem with \nn s are given by the class of \textbf{universal approximation} theorems, first proved by \citet{citeulike:3561150}. An introductory instance is given by \citet{dlb}

\begin{theorem}[UAT for single-layer-FFN]
\label{thm:uat}
A feedforward network with a single layer is sufficient to represent any function, but the layer may be unfeasibly large and may fail to learn and generalize correctly.
\end{theorem}

The theorem assures that, if the problem can be reduced to a mapping (function) between sets, then, given a suitable architecture, it is solvable. The difficulty relies in the architecture search.

\subsection{\xmakefirstuc{\nn}s}
\label{subsec:background_nn}

\xmakefirstuc{\nn}s are a class of models that consist of

\begin{itemize}
    \item \textit{neurons} which hold a value
    \item (weighted) \textit{synapses} which connect multiple neurons
    \item \textit{activations} managing the \textit{result} of a linear combination of neurons, yielding a \textit{final} result. Usually these are divided into two classes, based on the their linearity property
\end{itemize}

\xmakefirstuc{\nn}-based models are \textit{trained} through gradient-based methods via the backpropagation algorithm (the matter of who invented backpropagation is subject to critic, so we remind to \citet{Schmidhuber_backpropagation} for a detailed historical description). Overall, the recent success of \nn-based models is due to (apart from hardware reasons, see \citet{hardware_Lottery})

\begin{itemize}
    \item the success in (stable) training (using normalization tecniques and without) due to suitable optimization algorithms
    \item the ability to extract good representations (even without employing architecture-search methods)
\end{itemize}

Given the high number of neurons in a \nn{}, usually the \nn{} is abstracted as a combination of \textit{blocks}, connected via various types of transitions (\eg{} activations, connections ...). In the following sections, we will describe some of the most salient points and tecniques used in \nn{} architectures. We omit a full and in-depth study of them, since it's out of the scope of this thesis, and we will remind to trustable references when appropriate.\\

\subsection{Fully-connected (dense) blocks}
\label{subsec:background_dense}

When all inputs from one layer are connected to every activation of the next layer, the layer is commonly called \textbf{fully-connected} (\aka{} \textbf{dense}). Since the output is a linear combination of the input, unless a non-linear activation is used, these blocks can model just linear funtions.

\subsection{Residual blocks}
\label{subsec:background_residual}

Empirically, a \nn{} may find it difficult to learn the identity transformation $x = f(x)$. To improve on this side, we \textit{add} the input at the end of the plain output ( thus making the \nn{} learn the \textit{residual} mapping, \ie{} $f(x) - x$) as shown in \prettyref{fig:res_block}

\showFig{\fullImgWidth}{\imgDir/res_block.png}{A regular block (left) and a residual block (right), \citet{d2l}}{res_block}

Stacking multiple blocks results in building a \textit{residual} \nn.

\subsection{Skip connections}
\label{subsec:background_skips}

Skip connections can be also thought as residual connections, but usually connect the input of a layer with the output of a much further layer. They are commonly found in U-Net-like architectures (see \citet{paper:unet} for one of the first successful usage).

\subsection{\xmakefirstuc{\cnn}s}
\label{subsec:background_cnn}

A \cnn{} performs the convolution of the input image (multiple times) to find a low-dimensional embedding of the high-dimensional pixel-based image space. Usually, (non-)linear activations are employed in-between convolutional layers.\\

The translation invariance property of the convolution kernel is beneficial in \cv{} tasks, since each part of the image (supposedly) should be \textit{treated} in the same way.

\subsection{Optimization}
\label{subsec:background_opt}

Given the output of a neural network (usually a scalar, vector, or more generally a tensor) and a loss (\ie{} difference between the output and a reference value), if the loss is differentiable \wrt{} all parameters (\ie{} the neurons) of the \nn{} we can apply gradient descent methods to find local minima of the loss, thus minimizing the error and optimizing the found solution.\\

\subsection{Regularization}
\label{subsec:background_regularization}

Broadly speaking, in terms of optimization, a \textbf{regularization} tecnique aims to add some bias to either solve an ill-posed problem or to prevent overfitting (\ie{} when a model memorizes training data by heart and cannot generalize to evaluation data). A standard regularization tecnique in \nn{} training domain, is measuring the \textit{complexity} of the learnt function. \href{https://en.wikipedia.org/wiki/Occam's_razor}{Ideally} the \nn{} should learn the \textit{simplest} function, where the simplicity is measured as the norm of the coefficients learnt. Indeed, given the \nn{} parameters as $\mathbf{w}$, the regularization tecnique \textbf{weight decay} introduces the following loss component (the division by $2$ is just a convention, resulting from the derivative of a quadratic function)

\begin{equation}
\label{eq:weight_decay}
    \frac{\lambda}{2} \|\mathbf{w}\|^2
\end{equation}

For $\lambda = 0$, we employ the original loss function, and for $\lambda > 0$ we restrict the size of $\| \mathbf{w} \|$.

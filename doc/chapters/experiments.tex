\chapter{Evaluation}
\label{ch:empirical_results}

This chapter lists the empirical results and evaluations of the proposed method \wrt{} the dataset.

\section{Dataset}
\label{sec:data_dataset}

Inspection of the data is a critical part in every \dl{} pipeline (\citet{The_Recipe}) so this section is devoted to describe the dataset we used to evaluate our method: Human3.6M. To see what kind of other dataset we could have used, please refer to \prettyref{sec:future}.

We opted to use H3.6M dataset since it's the most widely compared one and fits our assumptions:

\begin{itemize}
    \item synchronous cameras
    \item not monocular, many cameras
    \item single human in the field of view of any camera
\end{itemize}

Moreover \textit{nomen omen}: the dataset consists of more than 3 million frames, quite a large (enough) number to test our method. As described in \citet{H36M} the dataset comprises 5 females and 6 males subjects, recorded by 4 cameras, while conducting a \say{diverse set of motions and poses encountered as part of typical human activities (taking photos, talking on the phone, posing, greeting, eating, etc.)}. A sample of images contained in the dataset can be found in \prettyref{sec:supp_data}, especially \prettyref{fig:data_h36m}. However, one can argue that the dataset does not represent a real-world scenario for the following reasons:

\begin{itemize}
    \item cameras are approximately evenly spaced in a circumference whose center is the human: the angle between 2 consecutive cameras is approximately $\frac{\pi}{2}$ radians
    \item cameras are placed way above the human, approximately at same height and all looking \textit{down} (\ie{} at the pavement) with similar angle
    \item mean distance between human subject and cameras is approximately $5$ meters
    \item background (\ie{} what's not the human in the image) is not changing during recording process, thus (probably) leading to bias (see \prettyref{sec:Discussion_bias}) in camera rotation retrieving
    \item background is unusually bare (when compared to real-world backgrounds)
    \item images have a resolution of approximately $1000 \times 1000$ pixels, thus not allowing for fine details to be apparent
    \item human subject is clearly wearing infra-red marks (to be used by infra-red sensors to automatically gather location of the joints) which may be too evident and cause the model to retrieve the position of the marks, rather than the human joints
\end{itemize}

\section{Training/test set}
\label{sec:data_tt_set}

Usual training/test split does a poor job of splitting the dataset: any $x$ \% of the whole dataset includes all the different people in the dataset (\aka{} subjects), thus training a method on it will bias the method towards the training subjects. Splitting it by subject is thus considered the \say{standard protocol}:

\begin{itemize}
    \item we train the model on subjects $1, 6, 7, 8$
    \item we test it on subjects $9, 11$
\end{itemize}

We note that every subject performs every action, thus the only difference between training and testing data is the actual human performer (and very slight differences in camera setup due to noise).

Moreover, since the amount of data is big and wasting resources towards achieving diminishing returns, we opted for

\begin{itemize}
    \item training the model on 10 \% of the (training) data
    \item testing it on a third of the (testing) data; other comparable approaches \say{follow standard protocols and evaluate only on every 64th frame} (see \citet{CanonPose})
\end{itemize}

The following sections describe the experiments pipeline dividing them based on the data used. We don't mention supervised-training results since

\begin{itemize}
    \item it's no surprise they have been all successful
    \item they have been used as guidance towards selecting way of solving problems, \eg{} we studied the comparison of training the variants using Rodrigues \vs{} \citet{6d_mat_param} parametrization
    \item they do not represent a valid heuristic for a method that should work in the wild
\end{itemize}

\section{\xmakefirstuc{\vone}}
\label{sec:empirical_results_var1}

\xmakefirstuc{\vone} employs \texttt{RotoTransNet} to estimate camera parameters from 2D \kps{} detections and reconstructs the pose via triangulation (using DLT). This section summarizes the scenarios this variant has been applied to.

\subsection{Weakly supervised setting}
\label{subsec:empirical_results_v1_semi}

We considered training \texttt{RotoTransNet} using little and indirect supervision. To prepare data for these scenarios we re-sampled the dataset (by moving the pelvis to the origin) and camera information (by moving the camera field of view so that cameras look at the origin in world space). As mentioned in \prettyref{sec:pipeline_pre} we also re-sampled camera intrinsic matrices to be equal to a target intrinsic. Self-consistency loss ($\mathcal{L}_{\text{berHu}}$ on the re-projected pose) and $\mathcal{L}_{2}$ loss on the supervised data have been employed during the optimization step.\\

We have leveraged

\begin{itemize}
    \item 3D (world space) head location (it could have been any joint, other than pelvis) or
    \item length (in world space) of the left leg (it could have been also the right leg, or any other triplet of joints)
\end{itemize}

in the training and obtained respectively 23.2 and 7.1 MPJPE (mm), after optimally rotating the reconstructed pose. \prettyref{fig:v1_just_head} shows a sample and \prettyref{fig:3d_bones}a show the training history.

\subsection{Unsupervised setting}
\label{subsec:empirical_results_v1_unsuper}

In the unsupervised scenario, we pre-processed the dataset like in the weakly-supervised scenario (moving pelvis in origin, cameras look at origin) and in the post-processing step we additionally optimally re-scale the pose. In this scenario, just $\mathcal{L}_{\text{berHu}}$ on the re-projected pose has been optimized to obtain 12.3 PMPJPE (mm). \prettyref{fig:3d_unsuper} shows the training history and \prettyref{fig:unsuper_v1_vs_v2_history}a a validation sample.

\section{\xmakefirstuc{\vtwo}}
\label{sec:empirical_results_var2}

This variant trains the \texttt{Cam2camNet} model to perform triangulation (with DLT) in the master camera space. Pose can be re-projected in other views to provide self-consistency.

\subsection{Weakly supervised setting}
\label{subsec:empirical_results_v2_semi}

Just like in \prettyref{subsec:empirical_results_v1_semi} we supervised the length of the left leg bones and recovered 49.8 mm as MPJPE, after optimal rotation. \prettyref{fig:3d_bones}b shows a reconstructed sample.

\subsection{Unsupervised setting}
\label{subsec:empirical_results_v2_unsuper}

This scenario is the same as \prettyref{subsec:empirical_results_v1_unsuper} with the only difference of training \texttt{Cam2camNet} instead of \texttt{RotoTransNet}. The training history is plotted in \prettyref{fig:unsuper_v1_vs_v2_history}b. The validation metric is around 129.9 MPJPE (mm), calculated after Procrustes-alignment.

\section{Orthogonal-projection variant}
\label{sec:empirical_results_var_ortho}

In \vort{} we assume depth is negligible (\wrt{} pose width) and employ \texttt{RotoTransNet} with a weak-projection camera model. We just report the unsupervised setting scenario, which can be used to compare against \citet{CanonPose}.

\subsection{Unsupervised setting}
\label{subsec:empirical_results_ortho_unsuper}

In this scenario, we do not re-sample camera, nor data and apply self consistency losses in the orthogonal-projection in all views. In the post-processing steps, we Procrustes-align the reconstructed pose and obtain 25.3 PMPJPE (mm), as shown in \prettyref{fig:unsuper_orto}.

\section{Summary of results}
\label{sec:empirical_results}

This section summarizes the results obtained by comparing the different approaches used. Quantitative results are reported in \prettyref{tab:weak_results} and \prettyref{tab:unsuper_results}.

\subsection{Qualitative analysis}
\label{subsec:empirical_results_qual}

From a quick view of the results quality, we note that \vone{} performs much better than \vtwo{} in the unsupervised setting. \vort{} is competitive but assumes depth is negligible.\\

Additionally we note that the method appears to be flexible and adapt to different data conditions quite easily.\\

Loss optimization seems to not suffer from unstable conditions (mainly because of the regularization techniques deployed).\\

Overall, we find that

\begin{itemize}
    \item the re-sampling assumptions (pelvis in origin, cameras looking at origin) are not quite restrictive
    \item the quality of the reconstructed pose is on-par with current \sota{} approaches
\end{itemize}

Finally we note that, to our knowledge, the poor performance of \vtwo{} remains unexplained. We conjectured that retrieving a relative roto-translation appears to be much more complicated than foreseen.

\subsection{Quantitative analysis}
\label{subsec:empirical_results_quant}

As done previously, we divide quantitative results by data used during training

\begin{table*}[htp]
\centering
\rowcolors{2}{black!10}{black!40}
\begin{tabular}{*5l} \toprule
    resampling & data used & variant & post-processing & MPJPE (mm) \\ \midrule
    pelvis, camera & head 3D location & I & optimal rotation & 23.2 $\pm$ 2.4 \\
    pelvis, camera & left leg bones length & I & optimal rotation & 7.1 $\pm$ 1.2\\
    pelvis, camera & left leg bones length & II & optimal rotation & 49.8 $\pm$ 3.7 \\
\bottomrule
\hline
\end{tabular}
\caption{Results of proposed method, applied in different scenarios using weakly-supervision. MPJPE refers to the metric applied after post-processing steps.}
\label{tab:weak_results}
\end{table*}

\begin{table*}[htp]
\centering
\rowcolors{2}{black!10}{black!40}
\begin{tabular}{*4l} \toprule
    re-sampling & variant & post-processing & MPJPE (mm) \\ \midrule
    pelvis, camera & I & Procrustes align & 12.3 $\pm$ 0.5\\
    pelvis, camera & II & Procrustes align & 129.9 $\pm$ 5.7\\
    \xmark{} & orthogonal-projection & Procrustes align & 25.3 $\pm$ 1.1\\
\bottomrule
\hline
\end{tabular}
\caption{Results of proposed method, applied in different scenarios using no supervision. MPJPE refers to the metric applied after post-processing steps.}
\label{tab:unsuper_results}
\end{table*}
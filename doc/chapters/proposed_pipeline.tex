\chapter{3d \hpe{} pipeline}
\label{ch:prop_pipeline}

This chapter shows how the proposed method is instantiated in the full pipeline, from image processing to final result.

\section{Pre-processing}
\label{sec:pipeline_pre}

This first step involves processing each camera view, camera information and whole dataset. Regarding images we:

\begin{enumerate}
    \item either crop them around a \gt{} \say{bounding box} (when provided by \citet{paper:learnable_triangulation}) or crop them to a fixed resolution (usually $1024 \times 1024$ pixels). Note that the cropping forces us to change camera \gt{} information, as cropping and scaling corresponds to a change in the intrinsic matrix
    \item re-scale/re-crop the images so to have the same intrinsic matrix in all views. This is necessary due to possible ambiguities (see \prettyref{subsec:background_dolly})
\end{enumerate}

Images from all cameras are processed the same way. Regarding camera parameters we may apply a re-sampling of the dataset in order to have all cameras look at a specified point in space (usually the root joint, \ie{} the pelvis). Doing so involves \prettyref{eq:resampling_cam_ext}.\\

Additionally, extrinsic translation part can be scaled (since usually dataset contain $mm$ measures) in order to ease the \nn{} job. We found this to be of little help, so we settled for not employing this pre-processing step as to avoid bias.\\

To retrieve 2D \kps{} detections, multiple ways have been deployed:

\begin{itemize}
    \item using \gt{} \kps{} (after applying suitable noise)
    \item using the pre-trained backbone from \citet{paper:learnable_triangulation}. Triangulating the pose (via DLT) using this backbone yields near \sota{} results ($\sim 21.3mm$)
    \item using popular detectors \eg{} \citet{he2018mask}, \citet{chen2018cascaded}
\end{itemize}

We initially settled for using the pre-trained backbone from \citet{paper:learnable_triangulation}, but later turned to using \gt{} \kps{} since we wanted to evaluate the fitness of our method, not relying on any other possible source of error.\\

Pre-processing the whole dataset is necessary since we need to give a coordinate reference frame. We opted for the standard pre-processing, which entails moving the root joint to the origin.\\

2D \kps{} detections have to be processed before being fed to our \nn{}, and this steps consists of usual normalization techniques:

\begin{itemize}
    \item normalization by Frobenius norm
    \item normalization by maximum (among all views in batch of one subject) Frobenius norm
    \item normalization by distance to root joint (\ie{} pelvis)
    \item division by a fixed hyper-parameter
\end{itemize}

We found that Frobenius-normalizing works the best. Additionally, we can apply a translation to the \kps{} coordinates, given by the location of the root joint in order to have a \say{common rotation point for all 3D predictions}, just like in \citet{CanonPose}.

\section{Training regime}
\label{sec:pipeline_train}

We opted to use the widely known Adam loss optimization algorithm (\citet{adam_optim}), combined with weight decay (\citet{adam_optim_weight_decay}). We found that gradient clipping (as first introduced by \citet{grad_clipping}) helps with learning stability.

\section{Post-processing}
\label{sec:pipeline_post}

Depending on the learning setting (supervised or not), we employ different post-processing steps:

\begin{itemize}
    \item \say{since a multi-view self-supervised setting does not contain metric data, we adjust the scale of our predictions before calculating the MPJPE}, as in \citet{CanonPose}. We additionally apply the optimal rotation, so what we really calculate is denoted formally as PMPJPE
    \item in the weakly-supervised setting we just rotate the pose, as the scale should get learned correctly
    \item in full-supervision setting, we omit post-processing
\end{itemize}

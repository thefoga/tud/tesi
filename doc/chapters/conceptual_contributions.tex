\chapter{Learning camera information}
\label{ch:conc_contribs}

This chapter delineates the proposed method (from a theoretical point of view). For the discussion of empirical results see \prettyref{ch:empirical_results}.

\section{Genesis}
\label{sec:conc_contribs_genesis}

\xmakefirstuc{\pe{}} can be split into 2 problems:

\begin{itemize}
    \item 2D joint detections, \ie{} detecting joints in the 2D input images
    \item 3D lifting, \ie{} leveraging the 2D detections in image space to reconstruct the 3D pose in world space
\end{itemize}

The model we propose deals with the second problem: lifting the 2D detections.

\begin{assumption}[detections]
\label{ass:detections}
    We assume to have access to 2D detections of the joints in image space. This, however, it's not a limiting assumption, since either we can use a pre-trained joint detector backbone (\eg{} \citet{he2018mask}, \citet{chen2018cascaded}) or take the \gt{} detections (after applying suitable noise).
\end{assumption}

Predicting a pose from scratch (\ie from 2D detections) is hard for a \nn since, at the same time, the model has to

\begin{itemize}
    \item lift the 2D detections from image space to 3D world space
    \item triangulate using all views (\ie{} cameras)
\end{itemize}

Therefore we opted for a \textit{guided} pose reconstruction where we

\begin{enumerate}
    \item predict camera information (extrinsic matrix)
    \item triangulate (via DLT, see \prettyref{sec:background_pure_cv}) using all views (\ie cameras)
\end{enumerate}

so we are concerned with \textit{only} solving the problem of retrieving camera information from a point-cloud. Overall

\begin{itemize}
    \item the input $x$ to the model should be a (batched) tensor containing information regarding the batch detections, \ie $x \in \mathbb{R} ^ {B \times C \times J \times 2}$, where
    \begin{itemize}
        \item $B$ is the batch dimension
        \item $C$ is the number of views
        \item $J$ is the number of body joints
    \end{itemize}
    \item since the input is a point-cloud, usual \cnn{} models are not suitable for the task, so the inner layers of the \nn{} consist of linear transformations (dense layers)
    \item the output should be the camera information (matrices)
\end{itemize}

Overall the method that we developed is not a new idea, nor an algorithm. We applied the \textit{divide-et-impera} approach on the \textit{big} \pe{} problem and combined the solutions of smaller, manageable problems. In particular, we reduced \pe{} to camera rotation estimation and applied (one of) the best approach in the field.

Our high-level approach is

\begin{enumerate}
    \item construct an abstract representation of point-cloud through a backbone that transforms the 2D detections to a (long) vector
    \item predict rotation part of the extrinsic matrices from that representation
    \item separately, predict translation part of the extrinsic matrices
    \item combine them to have the full roto-translation (extrinsic) matrix of each camera
\end{enumerate}

What we achieved is, another subdivision of the problem (predicting separately rotations and translation) which makes our model modular and debuggable easily. Moreover we continue to consider smaller (and easier) problems so we can follow the best approach for each one.

\section{Model}
\label{sec:conc_contribs_model}

Regarding the roto-translation model, we make use of a simple building block (called \textbf{MLPResNet}), as shown in \prettyref{fig:archs-MLPResNet}:

\showFig{40mm}{\imgDir/archs-MLPResNet.png}{\texttt{MLPResNet} block}{archs-MLPResNet}

It consists of

\begin{itemize}
    \item an initial fully connected layer (in order to upscale the input to a manageable size
    \item a series of residual blocks (batch-normaliz-ed to improve speed of convergence with deep networks, see \citet{batchnorm_no_int_cov})
    \item a final (possibly activated) fully connected layer to scale the output to the required feature size
\end{itemize}

We emphasize the fact that we chose this bottleneck because it has proven effective in our empirical evaluations and it has solid theoretical foundations. We evaluated the following other approaches:

\begin{itemize}
    \item a dense network (with simple fully connected layers inside), but we found it performed poorly and couldn't extract meaningful features from the input
    \item encoder-decoder-style network (with eventual skip connections from \prettyref{subsec:background_skips}), but it was difficult to perform stable training with it
\end{itemize}

\subsection{Roto-translation estimation}
\label{subsec:conc_contribs_Rt}

Specifically

\begin{itemize}
    \item in order to retrieve a rotation matrix $R \in SO(3)$ we follow \citet{6d_mat_param} approach and apply a series of linear transformations (interleaved with non-linear neurons activations) to the abstract representation to get a vector $\in \mathbb{R}^6$ representing a rotation matrix in 3D world
    \item to estimate the translation part of the extrinsic matrix we (just) apply a series of dense layers to retrieve a vector $\in \mathbb{R}^3$
\end{itemize}

\subsection{Architecture}
\label{subsec:conc_contribs_arch}

We consider 2 version of the model, depending on the space where we perform the DLT:

\begin{itemize}
    \item \textbf{\vone} performs it in world space
    \item \textbf{\vtwo} estimates the extrinsic of just one (random) camera (that we denote by \textbf{master}) and the relative roto-translation matrices from that camera to all other. The we perform DLT in the master camera space
\end{itemize}

Overall this makes the \nn{} end-to-end differentiable and trainable through the backpropagation algorithm.

\subsection{\xmakefirstuc{\vone}}
\label{subsec:conc_contribs_var1}

In this variant the flow of information, from 2D detection to camera information is the same for each camera. Overall the model looks like \prettyref{fig:archs-RotoTransNet}:

\begin{itemize}
    \item the backbone is a deep (6 layers) based on the \texttt{MLPResNet} block
    \item predicting the rotation part relies on extracting a preliminary representation with a shallow \texttt{MLPResNet}, then building the actual rotation matrix using one of 2 parametrization considered:
    \begin{itemize}
        \item 6D parametrization, as motivated in \prettyref{subsec:background_repr_6d}
        \item angle-axis representation (\ie{} a \texttt{Rodrigues} block as in \citet{CanonPose})
    \end{itemize}
    \item translation vector is directly regressed via a \texttt{MLPResNet} block
\end{itemize}

\showFig{40mm}{\imgDir/archs-RotoTransNet.png}{\texttt{RotoTransNet} architecture}{archs-RotoTransNet}

\subsection{\xmakefirstuc{\vtwo}}
\label{subsec:conc_contribs_var2}

We build the \vtwo{} model on top of \vone{} information flow but we add skip connections from the detections of each camera to the corresponding representation. The model looks like \prettyref{fig:archs-Cam2camNet}:

\showFig{40mm}{\imgDir/archs-Cam2camNet.png}{\texttt{Cam2camNet} architecture}{archs-Cam2camNet}

\begin{itemize}
    \item an initial representation is learned by a deep module consisting of consecutive \texttt{MLPResNet} blocks
    \item in order to predict the roto-translation of the master camera, a \texttt{MLPResNet} block acts upon the master camera input (via a residual connection) and the initial representation
    \item as for predicting the relative roto-translations, we call a \texttt{MLPResNet} block on the other cameras input (via a residual connection) and the initial representation
\end{itemize}

\subsection{\xmakefirstuc{\vort}}
\label{subsec:conc_contribsvar_ortho}

In this variant, we assume (specifically an orthogonal projection, see \prettyref{sec:background_cam_other_proj}) cameras, thereby avoiding the need to estimate the translation vector. We use a variant of \texttt{RotoTransNet} (where we don't estimate the translation) and apply it to triangulate in world space (like in \prettyref{subsec:conc_contribs_var1}). Obviously, in this case, the predicted translation is useless and just self-consistent in all camera views.

\section{Loss}
\label{sec:conc_contribs_Loss}

We employ a large number of intermediate loss functions, and the final loss is just the summation over all the losses. Starting with usual losses definition, this section describes all losses used, dividing them in categories based on the source of data used.\\

Given 2 vectors $(x, y)$ each $\in \mathbb{R}^n$, we define

\begin{itemize}
    \item $\mathcal{L}_{1}(x, y) = \sum_{i=1}^n{ \mid x_i - y_i \mid }$, \ie{} the sum of differences between samples and relative reference value
    \item $\mathcal{L}_{2}(x, y) = \sum_{i=1}^n{ (x-y) ^ 2 }$, \ie{} the sum of squared differences between samples and relative reference value
    \item $\mathcal{L}_{\text{MSE}}(x, y) = \frac{\mathcal{L}_{2}}{n}$, \ie{} the mean $\mathcal{L}_{2}$
    \item 
    \begin{equation}
    \label{eq:l_MSESmooth}
        \mathcal{L}_{\text{MSESmooth}, t, \alpha, \beta}(x, y) = \begin{cases}
            (x-y) ^ {2 \alpha \cdot t ^ {\beta}} &\text{if} \quad (x-y) ^ 2 > t\\
            (x-y) ^ 2 &\text{else}
        \end{cases}
    \end{equation} as used in \citet{paper:learnable_triangulation} (with $\alpha, \beta = 0.1, 0.9$). Take a look at \prettyref{fig:loss_msesmooth} for a visualization of how it behaves under different set of parameters.
\end{itemize}

\subsection{With \gt{} information}
\label{subsec:conc_contribs_Loss_super}

Although not used in training the model, \textit{supervised} losses (\ie{} calculated using \gt{} data) comprise

\begin{itemize}
    \item geodesic error (\ie{} \say{the minimal angular difference between two rotations}) as defined by \citet{6d_mat_param}
    \begin{equation}
    \label{eq:l_geodesic}
        \mathcal{L}_{\text{geodesic}}(M, M') = \cos ^ {-1} \left( \frac{P_{00} + P_{11} + P_{22} - 1}{2} \right)
    \end{equation}
    where $P = M \times {M'} ^ {-1}$, on the rotation part of the extrinsic matrices. Take a look at \prettyref{fig:geodesic_error} for a visualization.
    \item $\mathcal{L}_2$ loss on the (scaled) translation part of the extrinsic matrices
    \item $\mathcal{L}_{\text{MSESmooth}}$ on the projections of the 3D reconstructed pose in each camera (\ie{} in image space) and the 3D reconstructed pose. A comparison between these two possibilities of supervision (given \gt{} extrinsics) can be grasped from \prettyref{fig:l_2d_vs_3d_super}
\end{itemize}

\subsection{Withouth \gt{} information}
\label{subsec:conc_contribs_Loss_unsuper}

We adopt self-consistency losses on the projections of the reconstructed 3D pose, since we cannot self-supervise directly in world space. So firstly we normalize the projected pose and the input detections (by the Frobenius norm) and apply following losses:

\begin{itemize}
    \item Huber loss is defined by \citet{huber} to be
    \begin{equation}
    \label{eq:l_Huber}
        \mathcal{L}_{\text{Huber}, \delta}(x) = \begin{cases}
            \frac12 \cdot x ^ 2 &\text{if} \quad \mid x \mid \le \delta\\
            \delta \cdot (\mid x \mid - \frac12 \cdot \delta) &\text{else}
        \end{cases}
    \end{equation}
    Take a look at \prettyref{fig:loss_huber} for a plot. Note that the input consist of just 1 vector, so there is much flexibility in combining this loss with other \eg{} $\mathcal{L}_1$, $\mathcal{L}_2$
    \item \textit{inverted} Huber loss, \aka{} berHu defined by \citet{berhu_loss} as
    \begin{equation}
    \label{eq:l_berHu}
        \mathcal{L}_{\text{berHu}, L}(x) = \begin{cases}
            \mid x \mid &\text{if} \quad \mid x \mid \le L\\
            \frac{x ^ 2 + L ^ 2}{2L} &\text{else}
        \end{cases}
    \end{equation}
    \prettyref{fig:loss_berhu} compares it with $\mathcal{L}_{1}$ and $\mathcal{L}_{2}$ losses (starting from the same difference of values)
    \item
    \begin{equation}
        \mathcal{L}_{\text{sep}, \delta}(x, y) = \frac{1}{n^2} \sum_{i=1}^n{ \sum_{j \neq i}^{n}{ \max (0, \delta ^2 - \mid x_i - x_j \mid ^ 2) }}
    \end{equation}
    as described by \citet{paper:discovery_latent}, to \say{penalize two \kps{} if they are closer than a hyperparameter $\delta$ in 3D}. The calculation is done in 3D \say{to allow multiple \kps{} to occupy the same pixel location as long as they have different depths}
\end{itemize}

We experimented with all losses described above, but settled for the weighted (linear) combination of the following. In the supervised setting, we explored using

\begin{itemize}
    \item $\mathcal{L}_{\text{geodesic}}$ (on the rotation part) and $\mathcal{L}_{2}$ (on the translation part) between predicted and \gt{} extrinsics/roto-translation matrices. The benefit of using this loss (\vs{} other more standard ones) can be easily seen in \prettyref{fig:L2_VS_geo}
    \item $\mathcal{L}_{\text{MSESmooth}}$ on the 3D pose and each camera projection
\end{itemize}

while, when not supervising, we applied self-consistency constraints via the following

\begin{itemize}
    \item (normalized) $\mathcal{L}_{\text{berHu}}$ between input \kps{} detections and re-projected pose
    \item we tried leveraging $\mathcal{L}_{\text{sep}}$ to avoid trivial consistent solutions (\eg{} a null pose, where all joints are predicted to be in the same 3D point) but found it useless
    \item in \vtwo{} we can combine previous losses combination with a camera relative rotation constraint: we impose that the the predicted rotation $R'$ deriving from combining the rotation of the master camera $R_{\text{master}}$ and the inverse relative rotation of all other cameras $R_{\text{master } \to \text{camera}_i} ^ {-1}$ is the identity rotation $I \in SO(3)$
    \begin{equation}
    \label{eq:l_self_geo}
        \mathcal{L}_{\text{geodesic}}(R', I) = \mathcal{L}_{\text{geodesic}}(R_{\text{master}} \times R_{\text{master } \to \text{camera}_i} ^ {-1}, I)
    \end{equation}
    \item analogous reasoning can be applied to the translation part (by multiplying the whole extrinsic matrix) and using a (simple) $\mathcal{L}_{\text{2}}$ loss
\end{itemize}

\section{Hyper-parameters}
\label{sec:conc_contribs_hyper}

We find that

\begin{itemize}
    \item using a learning rate $\in [1e-3, 1e-5]$ does not really affect the quality of the results, just the speed of convergence
    \item using weight decay (as described in \citet{adam_optim_weight_decay}) helps with generalization (\prettyref{sec:Discussion_gener})
\end{itemize}

We have not conducted a in-depth search of hyper-parameters (\eg{} $L$ parameter of $\mathcal{L}_{\text{berHu}}$ was not automatically optimized, rather via ablation studies).

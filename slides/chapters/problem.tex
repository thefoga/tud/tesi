\section{Problem statement}

\subsection{Model}

\begin{frame}{Scene}{How to model this? A little bit of background ..}
\centering
\begin{columns}

    \begin{column}{.48\textwidth}
    \centering
    \adjincludegraphics[width=.4\linewidth]{\imgDir/multiview/54138969.jpg} \adjincludegraphics[width=.4\linewidth]{\imgDir/multiview/58860488.jpg}\\
    
    \adjincludegraphics[width=.4\linewidth]{\imgDir/multiview/55011271.jpg} \adjincludegraphics[width=.4\linewidth]{\imgDir/multiview/60457274.jpg}
    \end{column}
\end{columns}
\end{frame}

\begin{frame}{Scene}{Epipolar geometry}
\centering

\begin{figure}
\adjincludegraphics[width=.6\linewidth]{\imgDir/1920px-Epipolar_geometry.svg.png}
\caption{epipolar geometry, \copyright{} Arne Nordmann}
\end{figure}
\end{frame}

\begin{frame}{Camera models}{Pinhole}
    \centering
    \begin{figure}
    \adjincludegraphics[width=.7\linewidth]{\imgDir/pinhole_model.png}
    \caption{pinhole camera projection model, \copyright{} PyTorch geometry}
    \end{figure}
    \end{frame}
    
\begin{frame}{Camera models}{Pinhole's $K$, $R \mid t$}
    \centering
    \begin{align}
    \label{eq:proj_camera}
    P &= \overbrace{K}^{\text{intrinsic matrix}} \times \overbrace{[R \mid  \mathbf{t}]}^{\text{extrinsic matrix}} \\ &=
    \overbrace{
        \underbrace{
            \left (
            \begin{array}{ c c c}
                1  &  0  & x_0 \\
                0  &  1  & y_0 \\
                0  &  0  & 1
            \end{array}
            \right )
        }_{\text{2D translation}}
        \times
        \underbrace{
            \left (
            \begin{array}{ c c c}
            f_x &  0  & 0 \\
                0  & f_y & 0 \\
                0  &  0  & 1
            \end{array}
            \right )
        }_{\text{2D scaling}}
        \times
        \underbrace{
            \left (
            \begin{array}{ c c c}
                1  &  s/f_x  & 0 \\
                0  &    1    & 0 \\
                0  &    0    & 1
            \end{array}
            \right )
        }_{\text{2D Shear}}
    }^{\text{intrinsic matrix}}
    \times
    \overbrace{
        \underbrace{
            \left ( \begin{array}{c | c}
            I & \mathbf{t}
            \end{array} \right )
        }_{\text{3D translation}}
        \times
        \underbrace{
            \left ( \begin{array}{c | c}
            R & 0 \\ \hline
            0 & 1
            \end{array} \right )
        }_{\text{3D rotation}}
    }^\text{extrinsic matrix}
    \end{align}
\end{frame}

\begin{frame}{Camera models}{others}

\begin{itemize}
    \item \textbf{orthogonal}
    
    \begin{center}
        \begin{figure}
            \adjincludegraphics[width=.35\linewidth]{\imgDir/weak_model.png}
            \caption{weak projection camera model, \copyright{} Standford CS231A}
        \end{figure}
    \end{center}

    \item \textbf{weak}
    
    \begin{center}
        \begin{figure}
            \adjincludegraphics[width=.25\linewidth]{\imgDir/orto_model.png}
            \caption{pinhole camera projection model, \copyright{} Standford CS231A}
        \end{figure}
    \end{center}
\end{itemize}
\end{frame}

\begin{frame}{Triangulation}{Ideal case ...}
\centering
\begin{figure}
\adjincludegraphics[width=.65\linewidth]{\imgDir/TriangulationIdeal.png}
\caption{triangulation (ideal case), \copyright{} KYN}
\end{figure}
\end{frame}

\begin{frame}{Triangulation}{... in real world}
\begin{figure}
\adjincludegraphics[width=.65\linewidth]{\imgDir/TriangulationReal.png}
\caption{triangulation (real world case) where noisy $\Rightarrow$ points $y'_1$ and $y'_2$ are detected (and used), \copyright{} KYN}
\end{figure}
\end{frame}

\begin{frame}{Triangulation}{Formal problem statement}

\begin{equation}
\label{eq:triangulation_formal}
X = \tau(x, x', P, P')
\footnote{\cite{hartley_zisserman_2004}, 12.2 (p. 312)}
\end{equation}

where

\begin{itemize}
    \item $\tau$ is some \textit{triangulator} method
    \item $X$ are the estimated (3D) points
    \item $P, P'$ are the projection matrices (extrinsic and intrinsic)
    \item $x, x'$ are the projected points, \ie{} $x = PX, x' = P' X$
\end{itemize}
\end{frame}

\begin{frame}{Triangulation}{Solution via DLT}

\begin{enumerate}
    \item calibrate cameras
    \item combine $x = PX, x' = P' X$ into $AX = 0$ (linear in $X$)
    \item eliminate scale factor by cross product
    \item (homogeneous) solution by SVD-ing $A = \mathbf {U\Sigma V^{T}}$ (eigenvalues $\sim$ re-projection error)
\end{enumerate}
\end{frame}

\section{Related work}

\begin{frame}{Related work}{Single-person 3D \hpe{} approaches}
\centering
\begin{figure}
\adjincludegraphics[width=.5\linewidth]{\imgDir/single_person.png}
\caption{Main classes of approaches for single-person 3D \hpe{}, \cite{dl_hpe_survey}}
\end{figure}
\end{frame}

\begin{frame}{Related work}{Pictorial structure model}

\begin{columns}
\begin{column}{.5\textwidth}
    \begin{figure}
    \adjincludegraphics[width=.75\linewidth]{\imgDir/pic_model_original.png}
    \caption{algebraic triangulation, \cite{paper:learnable_triangulation}}
    \end{figure}
\end{column}

\begin{column}{.5\textwidth}
    \begin{figure}
    \adjincludegraphics[width=.75\linewidth]{\imgDir/pic_struct_model.png}
    \caption{\say{graphical representation of the dependencies between the location of object parts (black nodes) and the image}, \cite{original_pic_struct_model}}
    \end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{Related work}{3D \hpe}
\centering
\begin{figure}
\adjincludegraphics[width=.5\linewidth]{\imgDir/sample_dlt.png}
\caption{algebraic triangulation, \cite{paper:learnable_triangulation}}
\end{figure}
\end{frame}

\begin{frame}{Supervised methods}{Lifting-based}
\begin{columns}
\begin{column}{.5\textwidth}
    \begin{itemize}
        \item using camera information ($R \mid t, K$)
        \item $\mathcal{L}$ directly on $3D$ (world space)
    \end{itemize}
\end{column}

\begin{column}{.5\textwidth}
    \eg{} approaches:
    \begin{itemize}
        \item \cite{paper:learnable_triangulation}
        \item \cite{epipol_transformer}
    \end{itemize}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{Unsupervised methods}{Lifting-based}
\begin{columns}
\begin{column}{.5\textwidth}
    \begin{itemize}
        \item camera information ($R \mid t, K$) \textbf{not} available
        \item cannot supervise $2D$ (image space), nor $3D$ (camera/world space)
        \item \textbf{need} to use multi-view consistencies (at least at training time)
        \item \textit{weak}: use geometry priors (\eg{} bone lenghts, orientation ...)
    \end{itemize}
\end{column}

\begin{column}{.5\textwidth}
    \eg{} approaches:
    \begin{itemize}
        \item \cite{CanonPose}
        \item \cite{epilopar_pose}
        \item \cite{Unsupervised_3D_Pose_Estimation}
    \end{itemize}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{Unsupervised methods in the wild}{Consequences}
    \begin{itemize}
        \item Dolly zoom effect $\Rightarrow$ need to use \textbf{fixed} $K$ \begin{figure}
            \adjincludegraphics[width=.1\linewidth]{\imgDir/res/fake_cam_rand.png}
            \caption{Random (uniformly-distributed Euler angles) camera system used for experiments}
            \end{figure}

        \item scale ambiguities: \begin{figure}
            \adjincludegraphics[width=.1\linewidth]{\imgDir/res/incam.png}
            \caption{Reconstructed pose in camera space in \colorbox{red}{red} \vs{} \gt{} pose in camera space in \colorbox{blue}{\textcolor{white}{blue}}}
            \end{figure}
        \item reconstruction must be in a (self-consistent) canonical space $\Rightarrow$ \textbf{MPJPE} is a meaningless metric (use \textbf{PMPJPE}, \textbf{NMPJPE})
    \end{itemize}
\end{frame}
cwd            := $(shell pwd)

update         := sudo apt -qq update
install        := sudo apt install -y --no-install-recommends

tex_compile    := pdflatex
bib_make       := bibtex

doc_file       := main
doc_folder     := doc
slides_file    := slides
slides_folder  := slides

.PHONY: all view

clean::
	rm -fv *.aux *.log *.bbl *.blg *.toc *.out *.lot *.lof *.atoc
	rm -fv *.bcf *.glo *.ist *.run.xml *-blx.bib *.acn *.synctex.gz
	rm -fv */*.aux */*.log */*.bbl */*.blg */*.toc */*.out */*.lot */*.lof */*.atoc
	rm -fv */*.bcf */*.glo */*.ist */*.run.xml */*-blx.bib */*.acn */*.synctex.gz
	rm -fv */*/*.aux */*/*.log */*/*.bbl */*/*.blg */*/*.toc */*/*.out */*/*.lot */*/*.lof */*/*.atoc
	rm -fv */*/*.bcf */*/*.glo */*/*.ist */*/*.run.xml */*/*-blx.bib */*/*.acn */*/*.synctex.gz

cleanall::
	$(MAKE) clean
	rm -fv *.pdf

install::
	@echo "sudo apt-get install texlive-latex-base texlive-fonts-recommended texlive-fonts-extra texlive-latex-extra texlive-science texlive-bibtex-extra biber"
	@echo "https://gitlab.com/thefoga/tud/tudscr -> ./doc

doc::
	cd $(doc_folder) && $(tex_compile) $(doc_file)
	cd $(doc_folder) && $(bib_make) $(doc_file)
	cd $(doc_folder) && $(tex_compile) $(doc_file)
	cd $(doc_folder) && $(tex_compile) $(doc_file)
	mv $(doc_folder)/$(doc_file).pdf .

slides::
	cd $(slides_folder) && $(tex_compile) $(slides_file).tex
	cd $(slides_folder) && biber $(slides_file)
	cd $(slides_folder) && $(tex_compile) $(slides_file).tex
	cd $(slides_folder) && $(tex_compile) $(slides_file).tex
	mv $(slides_folder)/$(slides_file).pdf .
